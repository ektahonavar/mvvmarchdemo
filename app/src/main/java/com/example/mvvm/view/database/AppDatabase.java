package com.example.mvvm.view.database;

import android.content.Context;
import android.util.Log;

import com.example.mvvm.model.People;
import com.example.mvvm.view.interfaces.PeopleDao;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {People.class}, version = 1,exportSchema = false)
@TypeConverters(RoomTypeConverter.class)
public abstract class AppDatabase extends RoomDatabase {

    private static final String LOG_TAG = AppDatabase.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "peopledb";
    private static AppDatabase sInstance;

    public static AppDatabase getInstance(Context context) {
        if (sInstance == null) {
                Log.d(LOG_TAG, "Creating new database instance");
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        AppDatabase.class, AppDatabase.DATABASE_NAME)
                        .build();
        }
        Log.d(LOG_TAG, "Getting the database instance");
        return sInstance;
    }
    public abstract PeopleDao peopleDao();
}
