package com.example.mvvm.view.interfaces;

import com.example.mvvm.model.People;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface PeopleDao {

    @Query("Select * from people")
    List<People> getPeopleList();

    @Insert
    void insert(List<People> people);

    @Query("DELETE FROM people")
    void delete();

    @Update
    void update(People people);

}
