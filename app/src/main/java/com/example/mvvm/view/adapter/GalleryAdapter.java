/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.mvvm.view.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.mvvm.R;
import com.example.mvvm.databinding.ItemGalleryBinding;
import com.example.mvvm.viewmodel.ItemGalleryViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.PeopleAdapterViewHolder> {

    ItemGalleryBinding binding;
    Context context;
    private List<String> strlist=new ArrayList<>();
    public GalleryAdapter()
    {
        this.strlist = new ArrayList<String>();
    }

    @Override
    public PeopleAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context=parent.getContext();
        binding=DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.item_gallery, parent, false);
        return new PeopleAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(PeopleAdapterViewHolder holder, int position) {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((AppCompatActivity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels / 3;

        int deviceheight = devicewidth ;

        binding.ivImg.getLayoutParams().width = devicewidth;

        //if you need same height as width you can set devicewidth in holder.image_view.getLayoutParams().height
        binding.ivImg.getLayoutParams().height = deviceheight;

        holder.bind(strlist.get(position));

    }


    @Override
    public int getItemCount() {
        return strlist.size();
    }

    public void setGalleryList(List<String> strlist) {
        this.strlist = strlist;
        notifyDataSetChanged();
    }

    static class PeopleAdapterViewHolder extends RecyclerView.ViewHolder {
        ItemGalleryBinding binding;

       public PeopleAdapterViewHolder(ItemGalleryBinding binding) {
           super(binding.itemLayout);
           this.binding=binding;
          // tvText = (CustomTextView) itemView.findViewById(R.id.lbl_title);
       }

        void bind(String imgurl) {
            if (binding.getItemgalleryViewModel() == null) {
                binding.setItemgalleryViewModel(new ItemGalleryViewModel(itemView.getContext(), imgurl));
            } else {
                binding.getItemgalleryViewModel().setImageUrl(imgurl);
            }
        }
    }
}
