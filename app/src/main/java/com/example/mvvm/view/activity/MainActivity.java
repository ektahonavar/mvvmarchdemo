package com.example.mvvm.view.activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;

import com.example.mvvm.R;
import com.example.mvvm.view.fragment.GalleryFragment;
import com.example.mvvm.view.fragment.MainFragment;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {


    Toolbar toolbar;
    DrawerLayout drawer;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar=(Toolbar)findViewById(R.id.toolbar);


         drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        toggle.syncState();

        NavigationView navigationView=(NavigationView)findViewById(R.id.navigation_view);

        navigationView.getMenu().getItem(0).setChecked(true);
        toolbar.setTitle("Home");
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer,new MainFragment());
        fragmentTransaction.addToBackStack(MainFragment.class.getName());
        fragmentTransaction.commit();
        drawer.closeDrawer(GravityCompat.START);




        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
                switch (item.getItemId())
                {

                  case  R.id.nav_home:
                    toolbar.setTitle("Home");

                    fragmentTransaction.replace(R.id.fragmentContainer,new MainFragment());
                      fragmentTransaction.addToBackStack(null);
                      fragmentTransaction.commit();
                    break;

                    case R.id.nav_gallery:
                    toolbar.setTitle("Gallery");
                        fragmentTransaction.replace(R.id.fragmentContainer,new GalleryFragment());
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    break;

                    case R.id.nav_send:
                    toolbar.setTitle("Send");
                    break;

                    case R.id.nav_share:
                    toolbar.setTitle("Share");
                    break;

                    case R.id.nav_slideshow:
                    toolbar.setTitle("Slideshow");
                    break;

                    case R.id.nav_tools:
                    toolbar.setTitle("Tools");
                    break;

                    default:
                        break;
                }

                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });

    }



}
