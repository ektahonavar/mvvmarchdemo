package com.example.mvvm.view.database;

import com.example.mvvm.model.Id;
import com.example.mvvm.model.Location;
import com.example.mvvm.model.Login;
import com.example.mvvm.model.Name;
import com.example.mvvm.model.Picture;
import com.google.gson.Gson;

import org.json.JSONObject;

import androidx.room.TypeConverter;

public class RoomTypeConverter {

    @TypeConverter
    public static Name fromString(String value) {
        return new Gson().fromJson(value, Name.class);
    }
    @TypeConverter
    public static String fromobj(Name list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    @TypeConverter
    public static Location fromlocString(String value) {
        return new Gson().fromJson(value, Location.class);
    }
    @TypeConverter
    public static String fromlocobj(Location list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    @TypeConverter
    public static Login fromlogString(String value) {
        return new Gson().fromJson(value, Login.class);
    }
    @TypeConverter
    public static String fromlogobj(Login list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    @TypeConverter
    public static Picture frompicString(String value) {
        return new Gson().fromJson(value, Picture.class);
    }
    @TypeConverter
    public static String frompicobj(Picture list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    @TypeConverter
    public static Id fromidString(String value) {
        return new Gson().fromJson(value, Id.class);
    }
    @TypeConverter
    public static String fromidobj(Id list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }
}
