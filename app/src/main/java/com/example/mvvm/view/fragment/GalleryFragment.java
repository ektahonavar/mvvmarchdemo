package com.example.mvvm.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mvvm.R;
import com.example.mvvm.databinding.FragmentGalleryBinding;
import com.example.mvvm.utils.RecyclerItemDecoration;
import com.example.mvvm.view.adapter.GalleryAdapter;
import com.example.mvvm.viewmodel.GalleryViewModel;


import java.util.Observable;
import java.util.Observer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class GalleryFragment extends Fragment implements Observer {

    public GalleryViewModel galleryViewModel;

    public FragmentGalleryBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_gallery, container, false);
        View view=binding.getRoot();
        initDataBinding();
        setupListGalleryView(binding.recyclerData);
        setupObserver(galleryViewModel);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void initDataBinding() {

        galleryViewModel = new GalleryViewModel(requireActivity());
        binding.setGalleryViewModel(galleryViewModel);
        galleryViewModel.setdata();
    }

    private void setupListGalleryView(RecyclerView recyclergallery) {

        GalleryAdapter adapter = new GalleryAdapter();
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        recyclergallery.addItemDecoration(new RecyclerItemDecoration(
                getResources().getDimensionPixelSize(R.dimen.photos_list_spacing),
                getResources().getInteger(R.integer.photo_list_preview_columns)));
        recyclergallery.setLayoutManager(manager);
        recyclergallery.setAdapter(adapter);
        recyclergallery.setHasFixedSize(true);

    }

    public void setupObserver(Observable observable) {
        observable.addObserver(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        galleryViewModel.reset();
    }

    @Override
    public void update(Observable observable, Object data)
    {
        if (observable instanceof GalleryViewModel) {
            GalleryAdapter galleryadapter = (GalleryAdapter) binding.recyclerData.getAdapter();
            GalleryViewModel galleryViewModel = (GalleryViewModel) observable;
            if (galleryadapter != null) {
                galleryadapter.setGalleryList(galleryViewModel.getstrList());
            }
        }
    }
}
