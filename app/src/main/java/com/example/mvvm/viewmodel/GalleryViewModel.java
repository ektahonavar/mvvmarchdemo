/**
 * Copyright 2016 Erik Jhordan Rey. <p/> Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at <p/> http://www.apache.org/licenses/LICENSE-2.0 <p/> Unless required by
 * applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 */

package com.example.mvvm.viewmodel;

import android.content.Context;
import android.view.View;

import com.example.mvvm.MyApplication;
import com.example.mvvm.model.People;
import com.example.mvvm.model.PeopleResponse;
import com.example.mvvm.model.Picture;
import com.example.mvvm.retrofit.ApiFactory;
import com.example.mvvm.retrofit.ApiService;
import com.example.mvvm.view.database.AppDatabase;
import com.example.mvvm.view.database.AppExecutors;
import com.example.mvvm.view.database.RoomTypeConverter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableInt;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class GalleryViewModel extends Observable {

    public ObservableInt Progressbar;
    public ObservableInt rvData;
    private List<String> strList;
    private Context context;
    private CompositeDisposable compDisposable = new CompositeDisposable();
    AppDatabase appDatabase;
    List<People> people = new ArrayList<>();
    boolean finish = false;

    public GalleryViewModel(Context activity) {

        this.context = activity;
        this.strList = new ArrayList<>();
        Progressbar = new ObservableInt(View.GONE);
        rvData = new ObservableInt(View.GONE);
        appDatabase = AppDatabase.getInstance(activity);
    }


    public void setdata() {
        initializeViews();
        //  fetchgallerylist();
        fetchlistfromdb();

    }


    //It is "public" to show an example of test
    public void initializeViews() {
        rvData.set(View.GONE);
        Progressbar.set(View.VISIBLE);
    }

    private void fetchgallerylist() {
        MyApplication peopleApplication = MyApplication.create(context);
        ApiService peopleService = peopleApplication.getPeopleService();

        Disposable disposable = peopleService.fetchPeople(ApiFactory.RANDOM_USER_URL)
                .subscribeOn(peopleApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<PeopleResponse>() {
                    @Override
                    public void accept(PeopleResponse peopleResponse) {

                        for (int i = 0; i < peopleResponse.getPeopleList().size(); i++) {
                            strList.add(peopleResponse.getPeopleList().get(i).getPicture().getLarge());
                        }

                        changePeopleDataSet(strList);
                        Progressbar.set(View.GONE);
                        rvData.set(View.VISIBLE);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {

                        throwable.printStackTrace();
                    }
                });

        compDisposable.add(disposable);
    }

    private void fetchlistfromdb() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                people.addAll(appDatabase.peopleDao().getPeopleList());

                for (int i = 0; i < people.size(); i++) {
                    strList.add(people.get(i).getPicture().getLarge());
                }
                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        changePeopleDataSet(strList);
                        Progressbar.set(View.GONE);
                        rvData.set(View.VISIBLE);
                    }
                });
            }
        });

    }


    private void changePeopleDataSet(List<String> peoples) {
        //   strList.addAll(peoples);
        setChanged();
        notifyObservers();
    }

    public List<String> getstrList() {
        return strList;
    }

    public void reset() {
        compDisposable.dispose();
        compDisposable = null;
        context = null;
    }
}
